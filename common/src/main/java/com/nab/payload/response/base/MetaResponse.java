package com.nab.payload.response.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaResponse {

    private int code;
    private List<Error> errors;
    private String service;
    private String extraError;

}
