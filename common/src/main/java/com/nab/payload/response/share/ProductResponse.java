package com.nab.payload.response.share;

import lombok.*;

import java.math.BigDecimal;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductResponse {

    private Long id;

    private String name;

    private String description;

    private String imageUrl;

    private String brand;

    private BigDecimal price;

    private String code;

    private String colour;

    private BigDecimal discountValue;

    private String categoryName;

}
