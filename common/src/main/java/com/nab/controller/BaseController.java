package com.nab.controller;

import com.nab.payload.response.base.BaseResponse;
import com.nab.payload.response.base.MetaResponse;

public class BaseController {

    protected <T> BaseResponse<T> ok(T r) {
        BaseResponse<T> baseResponse = new BaseResponse<>();
        baseResponse.setData(r);

        MetaResponse metaResponse = new MetaResponse();
        metaResponse.setCode(200);

        baseResponse.setMeta(metaResponse);
        return baseResponse;
    }

}
