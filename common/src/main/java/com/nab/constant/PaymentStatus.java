package com.nab.constant;

public enum PaymentStatus {
    PROCESSING, SUCCESS, FAILED
}
