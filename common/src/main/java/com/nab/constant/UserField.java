package com.nab.constant;

public class UserField {

    public static final String USER_ID = "user_id";
    public static final String ROLES = "roles";
    public static final String EMAIL = "email";
    public static final String USER_NAME = "user_name";

}
