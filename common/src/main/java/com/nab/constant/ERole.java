package com.nab.constant;

public enum ERole {
    ROLE_ADMIN, ROLE_USER
}
