package com.nab.constant;

public enum PaymentProvider {

    CASH, CREDIT_CARD, VISA

}
