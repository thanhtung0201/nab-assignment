package com.nab.constant;

public enum OrderStatus {
    PROCESSING, SUCCESS, FAILED
}
