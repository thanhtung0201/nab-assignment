package com.nab.constant;

public enum DiscountType {
    VAL, PERCENT
}
