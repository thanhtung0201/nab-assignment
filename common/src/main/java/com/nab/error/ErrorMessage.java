package com.nab.error;

public enum ErrorMessage {

    INVALID_USERNAME_OR_PASSWORD("INVALID_USERNAME_PASSWORD", "Username or password is incorrect"),
    USERNAME_READY_TAKEN("USER_NAME_EXISTS", "Username is already taken!"),
    EMAIL_READY_TAKEN("EMAIL_EXISTS", "Email is already in use!"),
    INVALID_USER_ID("INVALID_USER_ID", "Invalid user id"),
    REFRESH_TOKEN_EXPIRED("REFRESH_TOKEN_EXPIRED", "Refresh token was expired. Please make a new signin request"),
    REFRESH_TOKEN_INVALID("REFRESH_TOKEN_INVALID", "Invalid Refresh Token Request"),
    ADD_ROLE_ERROR("ADD_ROLE_ERROR", "Can not add user with role admin"),
    CHANGE_ROLE_ADMIN_INVALID("CHANGE_ROLE_ADMIN_INVALID", "Can not change role for admin"),
    CATEGORY_EXISTS("CATEGORY_EXISTS", "Category has already in system"),
    CATEGORY_ID_INVALID("CATEGORY_ID_INVALID", "Category id is invalid"),
    DISCOUNT_EXISTS("DISCOUNT_EXISTS", "Discount has already in system"),
    DISCOUNT_ID_INVALID("DISCOUNT_ID_INVALID", "Discount id is invalid"),
    PRODUCT_ID_INVALID("PRODUCT_ID_INVALID", "Product id is invalid"),
    PRODUCT_NOT_FOUND("PRODUCT_NOT_FOUND", "Product id is not found"),
    PRODUCT_EXISTS("PRODUCT_EXISTS", "Product has already in system"),
    AMOUNT_INVALID("AMOUNT_INVALID", "Amount invalid");

    private final String code;
    private final String message;

    ErrorMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
