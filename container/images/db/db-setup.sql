CREATE DATABASE IF NOT EXISTS user_service CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE DATABASE IF NOT EXISTS product_service CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE DATABASE IF NOT EXISTS order_service CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE user_service;

CREATE TABLE IF NOT EXISTS `user`
(
    `id`         bigint NOT NULL AUTO_INCREMENT ,
    `email`      varchar(50) NOT NULL ,
    `password`   varchar(100) NOT NULL ,
    `username`   varchar(50) NOT NULL ,
    `address`    text NULL ,
    `created_at` timestamp NOT NULL,

    PRIMARY KEY (`id`),
    UNIQUE KEY `AK1_Customer_CustomerName` (`email`)
) AUTO_INCREMENT=1 COMMENT='Basic information
about Customer';

CREATE TABLE IF NOT EXISTS `role`
(
    `id`   int NOT NULL ,
    `name` enum('ROLE_USER', 'ROLE_ADMIN') NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `user_role`
(
    `role_id` int NOT NULL ,
    `user_id` bigint NOT NULL ,

    PRIMARY KEY (`role_id`, `user_id`),
    KEY `fkIdx_283` (`role_id`),
    CONSTRAINT `FK_281` FOREIGN KEY `fkIdx_283` (`role_id`) REFERENCES `role` (`id`),
    KEY `fkIdx_287` (`user_id`),
    CONSTRAINT `FK_285` FOREIGN KEY `fkIdx_287` (`user_id`) REFERENCES `user` (`id`)
);


CREATE TABLE IF NOT EXISTS `refresh_token`
(
    `id`         bigint NOT NULL AUTO_INCREMENT ,
    `token`      varchar(100) NOT NULL ,
    `user_id`    bigint NOT NULL ,
    `expired_at` bigint NULL ,
    `created_at` timestamp NOT NULL ,

    PRIMARY KEY (`id`),
    UNIQUE KEY `AK1_Supplier_CompanyName` (`token`),
    KEY `fkIdx_153` (`user_id`),
    CONSTRAINT `FK_151` FOREIGN KEY `fkIdx_153` (`user_id`) REFERENCES `user` (`id`)
) AUTO_INCREMENT=1 COMMENT='Basic information
about Supplier';

CREATE TABLE IF NOT EXISTS `cart`
(
    `id`         bigint NOT NULL AUTO_INCREMENT ,
    `user_id` bigint NOT NULL ,
    `product_id` bigint NOT NULL ,
    `quantity`   int NOT NULL ,
    `created_at` timestamp NOT NULL ,

    PRIMARY KEY (`id`),
    KEY `fkIdx_363` (`user_id`),
    CONSTRAINT `FK_361` FOREIGN KEY `fkIdx_363` (`user_id`) REFERENCES `user` (`id`)
);

-- Masterdata
DELETE FROM role;
DELETE FROM user;
DELETE FROM user_role;
INSERT INTO role(id, name) VALUES(1, 'ROLE_ADMIN');
INSERT INTO role(id, name) VALUES(2, 'ROLE_USER');
-- Create_default_admin_user admin/admin
INSERT INTO user(id, email, password, username, address, created_at)
VALUES (1, 'admin@nab.com', '$2a$10$3TfC860AIUEWOMfRKJODnu4a5bP9orZHqtmmgQY0tijs6zF1caQpW', 'admin', 'NAB VietNam', CURRENT_TIMESTAMP());

INSERT INTO user_role(user_id, role_id) VALUES (1, 1);

-- ************************************** `Category`
USE product_service;

CREATE TABLE IF NOT EXISTS `category`
(
    `id`         int NOT NULL AUTO_INCREMENT,
    `name`       varchar(50) NOT NULL UNIQUE ,
    `created_at` timestamp NOT NULL ,
    PRIMARY KEY (`id`)
);

-- ************************************** `Discount`

CREATE TABLE IF NOT EXISTS `discount`
(
    `id`             int NOT NULL AUTO_INCREMENT,
    `discount_type`  enum('VAL', 'PERCENT') NOT NULL ,
    `name`           varchar(50) NOT NULL UNIQUE,
    `discount_value` decimal NOT NULL ,
    `created_at`     timestamp NOT NULL ,

    PRIMARY KEY (`id`)
);

-- ************************************** `Product`

CREATE TABLE IF NOT EXISTS `product`
(
    `id`          bigint NOT NULL AUTO_INCREMENT,
    `name`        varchar(100) NOT NULL UNIQUE,
    `discount_id` int NULL ,
    `category_id` int NOT NULL ,
    `description` text NULL ,
    `image_url`   varchar(200) NULL,
    `brand`       varchar(100) NOT NULL ,
    `price`       decimal(50) NOT NULL ,
    `code`        varchar(30) NOT NULL UNIQUE,
    `colour`      varchar(6) NULL ,
    `created_at`  timestamp NOT NULL ,

    PRIMARY KEY (`id`),
    KEY `fkIdx_328` (`category_id`),
    CONSTRAINT `FK_326` FOREIGN KEY `fkIdx_328` (`category_id`) REFERENCES `category` (`id`),
    KEY `fkIdx_334` (`discount_id`),
    CONSTRAINT `FK_332` FOREIGN KEY `fkIdx_334` (`discount_id`) REFERENCES `discount` (`id`)
);

CREATE INDEX IF NOT EXISTS idx_name ON product (name);
CREATE INDEX IF NOT EXISTS idx_brand ON product (brand);
CREATE INDEX IF NOT EXISTS idx_price ON product (price);
CREATE INDEX IF NOT EXISTS idx_colour ON product (colour);
CREATE INDEX IF NOT EXISTS idx_discount_id ON product (discount_id);
CREATE INDEX IF NOT EXISTS idx_category_id ON product (category_id);
CREATE INDEX IF NOT EXISTS idx_code ON product (code);

DELETE FROM product;
DELETE FROM category;
DELETE FROM discount;

-- Master data
INSERT INTO  category(name, created_at) values ('CAR', CURRENT_TIMESTAMP());
INSERT INTO  category(name, created_at) values ('PHONE', CURRENT_TIMESTAMP());
INSERT INTO  category(name, created_at) values ('CLOTHES', CURRENT_TIMESTAMP());
INSERT INTO  category(name, created_at) values ('SHOES', CURRENT_TIMESTAMP());
INSERT INTO  category(name, created_at) values ('BOOK', CURRENT_TIMESTAMP());


INSERT INTO  discount(discount_type, name, discount_value, created_at)
VALUES('VAL', 'Shopee 10/10', 200000, CURRENT_TIMESTAMP());
INSERT INTO  discount(discount_type, name, discount_value, created_at)
VALUES('PERCENT', 'Vietnam woman day 20/10', 15, CURRENT_TIMESTAMP());
INSERT INTO  discount(discount_type, name, discount_value, created_at)
VALUES('PERCENT', 'Vietnam national day 02/09', 35, CURRENT_TIMESTAMP());

INSERT INTO product(name, discount_id, category_id, description, image_url, brand, price, code, colour, created_at)
VALUES ('BMW-X7', 3, 1, 'BMW-X7', 'https://muaxegiatot.vn/wp-content/uploads/2020/03/gia-xe-bmw-x7-2020-2021-muaxegiatot-vn-thumb.jpeg',
        'BMW', 6889000000, 'BMW-X7', 'PINK', CURRENT_TIMESTAMP());
INSERT INTO product(name, discount_id, category_id, description, image_url, brand, price, code, colour, created_at)
VALUES ('Audi-Q5', 2, 1, 'Audi-Q5', 'https://i.xeoto.com.vn/auto/audi/q5/audi-q5-2020.png',
        'Audi', 2655830700, 'Audi-Q5', 'WHITE', CURRENT_TIMESTAMP());
INSERT INTO product(name, discount_id, category_id, description, image_url, brand, price, code, colour, created_at)
VALUES ('Porsche cayenne', 2, 1, 'Porsche cayenne', null,
        'Porsche', 12250000000, 'Porsche cayenne', 'BLACK', CURRENT_TIMESTAMP());
INSERT INTO product(name, discount_id, category_id, description, image_url, brand, price, code, colour, created_at)
VALUES ('Lexus LX570', 1, 1, 'Tech-laden is the best way to sum up Lexus’ year in the automobile industry', null,
        'Lexus', 8340000000, 'Lexus LX570', 'RED', CURRENT_TIMESTAMP());
INSERT INTO product(name, discount_id, category_id, description, image_url, brand, price, code, colour, created_at)
VALUES ('Lamborghini Aventador LP780-4', 1, 1, 'Lamborghini boasted record profits in 2020, delivering 7,430 cars globally with profits of $1.9 billion', null,
        'Lamborghini', 25000000000, 'LP780-4', 'YELLOW', CURRENT_TIMESTAMP());
INSERT INTO product(name, discount_id, category_id, description, image_url, brand, price, code, colour, created_at)
VALUES ('IPHONE 11 PROMAX', 3, 2, 'IPHONE 11 PROMAX', null,
        'APPLE', 20000000, 'IPHONE 11', 'GRAY', CURRENT_TIMESTAMP());
INSERT INTO product(name, discount_id, category_id, description, image_url, brand, price, code, colour, created_at)
VALUES ('SAMSUNG NOTE 10', 3, 2, 'SAMSUNG NOTE 10', null,
        'SAMSUNG', 12000000,'NOTE 10', 'WHITE', CURRENT_TIMESTAMP());
INSERT INTO product(name, discount_id, category_id, description, image_url, brand, price, code, colour, created_at)
VALUES ('ADIDAS', 2, 4, 'ADIDAS', null,
        'ADIDAS', 2340000, 'ADIDAS', 'WHITE', CURRENT_TIMESTAMP());



USE order_service;

-- ************************************** `Order`

CREATE TABLE IF NOT EXISTS `orders`
(
    `id`         bigint NOT NULL AUTO_INCREMENT,
    `status`     enum('PROCESSING','SUCCESS','FAILED') NOT NULL ,
    `amount`     decimal(50) NOT NULL ,
    `user_id`    bigint NOT NULL ,
    `created_at` timestamp NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `order_item`
(
    `id`         bigint NOT NULL AUTO_INCREMENT,
    `order_id`   bigint NOT NULL ,
    `product_id` bigint NOT NULL ,
    `quantity`   int NOT NULL ,

    PRIMARY KEY (`id`),
    KEY `fkIdx_378` (`order_id`),
    CONSTRAINT `FK_376` FOREIGN KEY `fkIdx_378` (`order_id`) REFERENCES orders (`id`)
);

-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;

-- ************************************** `Order_Payment`

CREATE TABLE IF NOT EXISTS `order_payment`
(
    `id`         bigint NOT NULL AUTO_INCREMENT,
    `order_id`   bigint NOT NULL ,
    `amount`     decimal(50) NOT NULL ,
    `provider`   enum('CASH', 'CREDIT_CARD', 'VISA') NOT NULL,
    `status`     enum('PROCESSING', 'SUCCESS', 'FAILED') NOT NULL,
    `created_at` timestamp NOT NULL ,

    PRIMARY KEY (`id`),
    KEY `fkIdx_399` (`order_id`),
    CONSTRAINT `FK_397` FOREIGN KEY `fkIdx_399` (`order_id`) REFERENCES orders (`id`)
);












