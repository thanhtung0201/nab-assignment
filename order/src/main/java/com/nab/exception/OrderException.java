package com.nab.exception;

import com.nab.error.ErrorMessage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderException extends RuntimeException {

    private ErrorMessage error;
    private String service;
    private int httpStatus;
    private String extraError;

    public OrderException(ErrorMessage error, String extraError) {
        this.error = error;
        this.service = "ORDER_SERVICE";
        this.httpStatus = 400;
        this.extraError = extraError;
    }

    public OrderException(ErrorMessage error) {
        this.error = error;
        this.service = "ORDER_SERVICE";
        this.httpStatus = 400;
    }

    public OrderException(ErrorMessage error, int httpStatus) {
        this.error = error;
        this.service = "ORDER_SERVICE";
        this.httpStatus = httpStatus;
    }
}
