package com.nab.exception;

import com.nab.payload.response.base.MetaResponse;
import com.nab.payload.response.base.Error;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ControllerAdvice
@Slf4j
public class OrderExceptionHandler {

    @ExceptionHandler(value
            = { OrderException.class})
    protected ResponseEntity<Object> handleUserException (
            Exception ex, WebRequest request) {
        log.error(ex.getMessage());
        if (ex instanceof OrderException) {
            OrderException orderException = (OrderException) ex;
            MetaResponse metaResponse = new MetaResponse();
            Error error = new
                    Error(orderException.getError().getCode(),
                    orderException.getError().getMessage(), orderException.getService()
            );
            metaResponse.setErrors(Arrays.asList(error));
            metaResponse.setCode(orderException.getHttpStatus());
            metaResponse.setService(error.getService());
            return ResponseEntity.status(orderException.getHttpStatus()).body(metaResponse);
        }
        else if (ex instanceof BadCredentialsException || ex instanceof AccessDeniedException) {
            String code = "";
            HttpStatus httpStatus;
            String message;
            if(ex instanceof BadCredentialsException) {
                code = "BAD_CREDENTIAL";
                httpStatus = HttpStatus.UNAUTHORIZED;
                message = "Invalid access token";
            } else {
                code = "FORBIDDEN";
                httpStatus = HttpStatus.FORBIDDEN;
                message = "Access deny to this resource";
            }
            Error error = new
                    Error(code,
                    message, "USER_SERVICE");
            return ResponseEntity.status(httpStatus)
                    .body(new MetaResponse(httpStatus.value(),
                            Arrays.asList(error),
                            "USER_SERVICE",
                            null));
        }
        else if(ex instanceof MethodArgumentNotValidException) {
            return handleValidatorMethodException((MethodArgumentNotValidException) ex);
        }
        else {
            return ResponseEntity.internalServerError().body("System Exception , Please contact admin");
        }

    }
    private ResponseEntity<Object> handleValidatorMethodException(MethodArgumentNotValidException ex) {
        MethodArgumentNotValidException exception = ex;
        List<Error> errorMessageResponses = new ArrayList<>();
        exception.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            Error err = new Error(fieldName, errorMessage, "ORDER_SERVICE");
            errorMessageResponses.add(err);
        });
        return ResponseEntity.badRequest()
                .body(new MetaResponse(HttpStatus.BAD_REQUEST.value(),
                        errorMessageResponses,
                        "ORDER_SERVICE",
                        null));
    }

}
