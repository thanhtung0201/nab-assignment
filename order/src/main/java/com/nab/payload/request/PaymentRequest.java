package com.nab.payload.request;

import com.nab.constant.PaymentProvider;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PaymentRequest {

    private BigDecimal amount;

    private PaymentProvider paymentProvider;

    private Long orderId;

}
