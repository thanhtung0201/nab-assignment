package com.nab.payload.response;

import com.nab.constant.OrderStatus;
import com.nab.constant.PaymentProvider;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class OrderResponse {

    private Long id;
    private OrderStatus orderStatus;
    private BigDecimal amount;
    private PaymentProvider paymentProvider;
    private List<ProductInfo> products;

}
