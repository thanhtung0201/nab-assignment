package com.nab.payload.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


@Getter
@Setter
@Builder
public class ProductResponse {

    private Long id;

    private String name;

    private String description;

    private String imageUrl;

    private String brand;

    private BigDecimal price;

    private String code;

    private String colour;

    private BigDecimal discountValue;

    private String categoryName;

}
