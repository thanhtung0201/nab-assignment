package com.nab.payload.response;

import lombok.Getter;
import lombok.Setter;

public @Getter
@Setter
class ProductInfo {
    private Long productId;
    private Integer quantity;
}
