package com.nab.repository;

import com.nab.entity.OrderPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderPaymentRepository extends JpaRepository<OrderPayment, Long> {


    @Query(nativeQuery = true, value = "SELECT * FROM order_payment ord WHERE ord.order_id IN(:orderIds)")
    List<OrderPayment> findAllByOrderIds(@Param(value = "orderIds") List<Long> orderIds);

}
