package com.nab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderStartedApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderStartedApplication.class, args);
    }
}
