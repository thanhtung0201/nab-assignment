package com.nab.controller;

import com.nab.entity.Order;
import com.nab.payload.request.OrderRequest;
import com.nab.payload.response.OrderResponse;
import com.nab.payload.response.base.BaseResponse;
import com.nab.service.OrderService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController extends BaseController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/make")
    public BaseResponse<String> createProduct(@Valid @RequestBody OrderRequest orderRequest) {
        Long userId = Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        Long orderId = orderService.makeOrder(orderRequest, userId);
        String successMessage = String.format("Create order %s successfully!", orderId);
        return ok(successMessage);
    }

    @GetMapping("/my-orders")
    public BaseResponse<List<OrderResponse>> myOrders() {
        Long userId = Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        return ok(orderService.myOrder(userId));
    }



}
