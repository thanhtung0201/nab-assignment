package com.nab.service;

import com.nab.payload.request.PaymentRequest;

public interface PaymentService {

    /**
     * Request make payment
     * @param request
     * @return
     */
    boolean makePayment(PaymentRequest request);

}
