package com.nab.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nab.client.HttpClient;
import com.nab.payload.response.share.ProductResponse;
import com.nab.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final HttpClient httpClient;

    private final ObjectMapper objectMapper;

    public ProductServiceImpl(HttpClient httpClient, ObjectMapper objectMapper) {
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;
    }


    @Override
    public List<ProductResponse> productList(List<Long> ids) {
        Object baseResponse = httpClient.getProductDetail(ids);
        return objectMapper.convertValue(baseResponse, new TypeReference<List<ProductResponse>>() { });
    }
}
