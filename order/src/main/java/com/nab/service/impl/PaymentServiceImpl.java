package com.nab.service.impl;

import com.nab.exception.PaymentGatewayException;
import com.nab.payload.request.PaymentRequest;
import com.nab.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Slf4j
public class PaymentServiceImpl implements PaymentService {

    @Override
    public boolean makePayment(PaymentRequest request) {
        if(request.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            log.warn("Invalid Payment Request , Amount can not be less than 0");
            return false;
        }
        try {
            makeCheckoutExternal();
            return true;
        } catch (PaymentGatewayException e) {
            log.error("Exception call payment gateway");
            return false;
        }
    }

    private void makeCheckoutExternal() throws PaymentGatewayException {
        log.info("Make call payment to payment gateway");
    }

}
