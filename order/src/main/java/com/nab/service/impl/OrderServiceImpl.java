package com.nab.service.impl;

import com.nab.constant.OrderStatus;
import com.nab.constant.PaymentProvider;
import com.nab.constant.PaymentStatus;
import com.nab.entity.Order;
import com.nab.entity.OrderItem;
import com.nab.entity.OrderPayment;
import com.nab.error.ErrorMessage;
import com.nab.exception.OrderException;
import com.nab.payload.request.OrderLineRequest;
import com.nab.payload.request.OrderRequest;
import com.nab.payload.request.PaymentRequest;
import com.nab.payload.response.OrderResponse;
import com.nab.payload.response.ProductInfo;
import com.nab.payload.response.share.ProductResponse;
import com.nab.repository.OrderPaymentRepository;
import com.nab.repository.OrderRepository;
import com.nab.service.OrderService;
import com.nab.service.PaymentService;
import com.nab.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final PaymentService paymentService;
    private final OrderPaymentRepository orderPaymentRepository;
    private final ProductService productService;


    public OrderServiceImpl(OrderRepository orderRepository, PaymentService paymentService, OrderPaymentRepository orderPaymentRepository, ProductService productService) {
        this.orderRepository = orderRepository;
        this.paymentService = paymentService;
        this.orderPaymentRepository = orderPaymentRepository;
        this.productService = productService;
    }

    @Override
    @Transactional
    public Long makeOrder(OrderRequest orderRequest, Long userId) {

        List<Long> productIds = orderRequest.getOrder()
                .stream()
                .map(OrderLineRequest::getProductId)
                .collect(Collectors.toList());
        List<ProductResponse> listProducts = productService.productList(productIds);

        validateProductInput(productIds, listProducts);

        Map<Long, ProductResponse> mapProductIdPrice = listProducts.stream().collect(Collectors.toMap(ProductResponse::getId, Function.identity()));

        Optional<BigDecimal> totalAmount = orderRequest.getOrder()
                .stream().map(or -> mapProductIdPrice.get(or.getProductId()).getPrice().multiply(BigDecimal.valueOf(or.getQuantity())))
                .reduce(BigDecimal::add);

        Order order = buildOrder(orderRequest, userId, totalAmount.get());

        Order result = orderRepository.save(order);
        PaymentRequest paymentRequest = buildPaymentRequest(totalAmount, result);
        boolean payment = paymentService.makePayment(paymentRequest);

        OrderPayment orderPayment = new OrderPayment();
        if(!payment) {
            log.error("Request payment fail for order {}", order.getId());
            order.setOrderStatus(OrderStatus.FAILED);
            orderPayment.setPaymentStatus(PaymentStatus.FAILED);
        } else {
            log.error("Request payment successfully for order {}", order.getId());
            order.setOrderStatus(OrderStatus.SUCCESS);
            orderPayment.setPaymentStatus(PaymentStatus.SUCCESS);
        }
        orderPayment.setAmount(totalAmount.get());
        orderPayment.setPaymentProvider(PaymentProvider.CASH);
        orderPayment.setOrder(order);

        orderPaymentRepository.save(orderPayment);
        return orderRepository.save(order).getId();
    }

    @Override
    public List<OrderResponse> myOrder(Long userId) {
        List<Order> orders = orderRepository.findAll();
        List<Long> orderIds = orders.stream()
                .map(Order::getId)
                .collect(Collectors.toList());
        List<OrderPayment> orderPayments = orderPaymentRepository.findAllByOrderIds(orderIds);
        Map<Order, OrderPayment> orderListMap = orderPayments.stream().collect(Collectors.toMap(OrderPayment::getOrder, Function.identity()));
        return orders.stream().map(or -> {
            OrderResponse orderResponse = new OrderResponse();
            orderResponse.setOrderStatus(or.getOrderStatus());
            orderResponse.setAmount(or.getAmount());
            orderResponse.setId(or.getId());
            List<ProductInfo> productInfos = or.getOrderItems().stream().map(ori -> {
                ProductInfo productInfo = new ProductInfo();
                productInfo.setProductId(ori.getProductId());
                productInfo.setQuantity(ori.getQuantity());
                return productInfo;
            }).collect(Collectors.toList());
            orderResponse.setProducts(productInfos);
            orderResponse.setPaymentProvider(orderListMap.get(or).getPaymentProvider());
            return orderResponse;
        }).collect(Collectors.toList());
    }

    private void validateProductInput(List<Long> productIds, List<ProductResponse> listProducts) {
        List<Long> idNotMatch = differenceProduct(productIds, listProducts.stream().map(ProductResponse::getId).collect(Collectors.toList()));
        if (idNotMatch.size() > 0) {
            String ids = Arrays.toString(idNotMatch.toArray());
            throw new OrderException(ErrorMessage.PRODUCT_ID_INVALID, "Product ID invalid " + ids);
        }
    }

    private PaymentRequest buildPaymentRequest(Optional<BigDecimal> totalAmount, Order result) {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(totalAmount.get());
        paymentRequest.setOrderId(result.getUserId());
        paymentRequest.setPaymentProvider(PaymentProvider.CASH);
        return paymentRequest;
    }

    private Order buildOrder(OrderRequest orderRequest, Long userId, BigDecimal totalAmount) {
        Order order = new Order();
        order.setOrderStatus(OrderStatus.PROCESSING);
        order.setUserId(userId);
        order.setAmount(totalAmount);
        List<OrderItem> orderItems = orderRequest.getOrder()
                .stream().map(or -> {
                    OrderItem orderItem = new OrderItem();
                    orderItem.setOrder(order);
                    orderItem.setProductId(or.getProductId());
                    orderItem.setQuantity(or.getQuantity());
                    return orderItem;
                }).collect(Collectors.toList());
        order.setOrderItems(orderItems);
        return order;
    }

    List<Long> differenceProduct(List<Long> source, List<Long> dest) {
        return source.stream()
                .filter(element -> !dest.contains(element))
                .collect(Collectors.toList());
    }
}
