package com.nab.service;

import com.nab.entity.Order;
import com.nab.payload.request.OrderRequest;
import com.nab.payload.response.OrderResponse;

import java.util.List;

public interface OrderService {

    /**
     * Request make an order
     * @param orderRequest
     * @param userId
     * @return Order Detail
     */
    Long makeOrder(OrderRequest orderRequest, Long userId);

    /**
     * Return all order user made
     * @param userId
     * @return
     */
    List<OrderResponse> myOrder(Long userId);

}
