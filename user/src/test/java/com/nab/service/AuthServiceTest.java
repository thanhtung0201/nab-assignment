package com.nab.service;

import com.nab.entity.User;
import com.nab.error.ErrorMessage;
import com.nab.exception.UserException;
import com.nab.payload.request.SignupRequest;
import com.nab.repository.UserRepository;
import com.nab.service.impl.AuthServiceImpl;
import com.nab.util.JwtUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class AuthServiceTest {

    @Spy
    @InjectMocks
    AuthServiceImpl authService;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    UserRepository userRepository;

    @Mock
    PasswordEncoder encoder;

    @Mock
    JwtUtils jwtUtils;

    @Mock
    RefreshTokenService refreshTokenService;

    @DisplayName("Required dependency")
    @Test
    public void mustHaveDependency() {
        Assertions.assertNotNull(authenticationManager);
        Assertions.assertNotNull(userRepository);
        Assertions.assertNotNull(encoder);
        Assertions.assertNotNull(jwtUtils);
        Assertions.assertNotNull(refreshTokenService);
    }

    @Test
    @DisplayName("Register User When Name is exists")
    void testRegisterUser_whenUserNameExists() {
        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setUsername("Username taken");
        when(userRepository.existsByUsername(Mockito.anyString())).thenReturn(true);
        UserException exception = assertThrows(UserException.class, () -> {
            authService.register(signUpRequest);
        });
        assertEquals(exception.getError(), ErrorMessage.USERNAME_READY_TAKEN);
    }

    @Test
    @DisplayName("Register User When Email is exists")
    void testRegisterUser_whenEmailExists() {
        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setEmail("Email Taken");
        when(userRepository.existsByUsername(Mockito.anyString())).thenReturn(false);
        when(userRepository.existsByEmail(Mockito.anyString())).thenReturn(true);
        UserException exception = assertThrows(UserException.class, () -> {
            authService.register(signUpRequest);
        });
        assertEquals(exception.getError(), ErrorMessage.EMAIL_READY_TAKEN);
    }

    @Test
    @DisplayName("Create User successfully")
    public void testCreateUser_Success()  {
        SignupRequest signupRequest = new SignupRequest();
        when(userRepository.existsByUsername(Mockito.anyString())).thenReturn(false);
        when(userRepository.existsByEmail(Mockito.anyString())).thenReturn(false);
        when(userRepository.save(any())).thenReturn(new User());
        Assertions.assertDoesNotThrow(() -> authService.register(signupRequest));
    }

}
