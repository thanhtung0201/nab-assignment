package com.nab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserStartedApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserStartedApplication.class, args);
    }
}
