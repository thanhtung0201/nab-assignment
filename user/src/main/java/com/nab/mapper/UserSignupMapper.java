package com.nab.mapper;

import com.nab.entity.User;
import com.nab.payload.request.SignupRequest;
import org.mapstruct.factory.Mappers;


@org.mapstruct.Mapper
public interface UserSignupMapper extends Mapper<SignupRequest, User> {

        UserSignupMapper MAPPER = Mappers.getMapper(UserSignupMapper.class);

}
