package com.nab.mapper;

import org.mapstruct.MappingTarget;

public interface Mapper<T, R> {
    R map(T source);

    void mapTo(T source, @MappingTarget R target);
}
