package com.nab.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AddCartRequest {

   private List<CartLineRequest> carts;

}


