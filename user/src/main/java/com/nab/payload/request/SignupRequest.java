package com.nab.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Parent;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class SignupRequest {

    @NotBlank
    @Size(max = 50)
    private String username;

    @NotBlank
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$",
            message= "Password must contain at least one digit, must contain at least one digit," +
                    "must contain at least one digit, must contain at least one special character like ! @ # & ( )," +
                    "must contain at least one special character like ! @ # & ( )")
    private String password;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    private String address;

}
