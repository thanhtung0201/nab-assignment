package com.nab.payload.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CartLineRequest {

    private Long productId;

    private Integer quantity;

}
