package com.nab.payload.response;

import com.nab.payload.response.share.ProductResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserCartResponse {

    private Long cartId;
    private Integer quantity;
    private ProductResponse productResponse;

}
