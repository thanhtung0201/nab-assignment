package com.nab.service;

import com.nab.payload.request.LoginRequest;
import com.nab.payload.request.SignupRequest;
import com.nab.payload.response.LoginResponse;

public interface AuthService {

    /**
     * Register
     * @param request {username, password, email, address}
     */
    void register(SignupRequest request);

    /**
     * Login
     * @param request {username, password}
     * @return
     */
    LoginResponse login(LoginRequest request);
}
