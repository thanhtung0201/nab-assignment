package com.nab.service;

import com.nab.payload.request.AddRoleRequest;

public interface RoleService {


    /**
     * Admin add role for user
     * @param addRoleRequest
     */
    void addRoleUser(AddRoleRequest addRoleRequest);

}
