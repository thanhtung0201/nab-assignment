package com.nab.service;

import com.nab.payload.request.AddCartRequest;
import com.nab.payload.response.UserCartResponse;

import java.util.List;

public interface UserService {

    /**
     * Add product to cart for shopping
     * @param request
     * @param userId
     */
    void addProductToCart(AddCartRequest request, Long userId);

    /**
     * Retrieve all cart of user
     * @param userId
     * @return
     */
    List<UserCartResponse> cart(Long userId);

}
