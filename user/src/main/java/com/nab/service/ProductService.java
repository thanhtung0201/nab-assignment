package com.nab.service;

import com.nab.payload.response.share.ProductResponse;

import java.util.List;

public interface ProductService {

    List<ProductResponse> productList(List<Long> ids);

}
