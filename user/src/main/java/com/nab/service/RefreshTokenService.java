package com.nab.service;

import com.nab.entity.RefreshToken;
import com.nab.payload.request.TokenRefreshRequest;
import com.nab.payload.response.RefreshTokenResponse;


public interface RefreshTokenService {

    /**
     * Create refresh token
     * @param tokenRefreshRequest
     * @return
     */
    RefreshTokenResponse refreshToken(TokenRefreshRequest tokenRefreshRequest);

    /**
     * Request new token
     * @param userId
     * @return
     */
    RefreshToken createRefreshToken(Long userId);

}
