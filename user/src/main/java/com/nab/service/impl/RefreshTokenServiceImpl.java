package com.nab.service.impl;

import com.nab.entity.RefreshToken;
import com.nab.entity.User;
import com.nab.error.ErrorMessage;
import com.nab.exception.UserException;
import com.nab.payload.request.TokenRefreshRequest;
import com.nab.payload.response.RefreshTokenResponse;
import com.nab.repository.RefreshTokenRepository;
import com.nab.repository.UserRepository;
import com.nab.service.RefreshTokenService;
import com.nab.util.JwtUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import java.time.Instant;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {


    private final Long refreshTokenDurationMs;

    private final RefreshTokenRepository refreshTokenRepository;

    private final UserRepository userRepository;

    private final JwtUtils jwtUtils;

    public RefreshTokenServiceImpl(@Value("${nab.app.jwtRefreshExpirationMs}") Long refreshTokenDurationMs,
                                   RefreshTokenRepository refreshTokenRepository,
                                   UserRepository userRepository, JwtUtils jwtUtils) {
        this.refreshTokenDurationMs = refreshTokenDurationMs;
        this.refreshTokenRepository = refreshTokenRepository;
        this.userRepository = userRepository;
        this.jwtUtils = jwtUtils;
    }


    @Override
    public RefreshTokenResponse refreshToken(TokenRefreshRequest tokenRefreshRequest) {
        String requestRefreshToken = tokenRefreshRequest.getRefreshToken();
        return this.findByToken(requestRefreshToken)
                .map(this::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    String token = jwtUtils.generateTokenFromUsername(user.getUsername());
                    return new RefreshTokenResponse(token, requestRefreshToken);
                })
                .orElseThrow(() -> new UserException(ErrorMessage.REFRESH_TOKEN_INVALID));
    }

    private Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    @Transactional
    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiredAt().compareTo(Instant.now().toEpochMilli()) < 0) {
            refreshTokenRepository.delete(token);
            throw new UserException(ErrorMessage.REFRESH_TOKEN_EXPIRED);
        }
        return token;
    }

    private User fetchUser(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if(!user.isPresent()) {
            throw new UserException(ErrorMessage.INVALID_USER_ID);
        }
        return user.get();
    }

    @Transactional
    @Override
    public RefreshToken createRefreshToken(Long userId) {

        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUser(fetchUser(userId));
        refreshToken.setExpiredAt(Instant.now().plusMillis(refreshTokenDurationMs).toEpochMilli());
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }


}
