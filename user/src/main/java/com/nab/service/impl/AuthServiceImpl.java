package com.nab.service.impl;

import com.nab.entity.RefreshToken;
import com.nab.entity.User;
import com.nab.error.ErrorMessage;
import com.nab.exception.UserException;
import com.nab.mapper.UserSignupMapper;
import com.nab.payload.request.LoginRequest;
import com.nab.payload.request.SignupRequest;
import com.nab.payload.response.LoginResponse;
import com.nab.repository.RoleRepository;
import com.nab.repository.UserRepository;
import com.nab.service.AuthService;
import com.nab.service.RefreshTokenService;
import com.nab.util.JwtUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;

    private final PasswordEncoder encoder;

    private final JwtUtils jwtUtils;

    private final RefreshTokenService refreshTokenService;


    public AuthServiceImpl(AuthenticationManager authenticationManager, UserRepository userRepository,
                           PasswordEncoder encoder, JwtUtils jwtUtils,
                           RefreshTokenService refreshTokenService) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;

        this.refreshTokenService = refreshTokenService;
    }

    @Override
    @Transactional
    public void register(SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            throw new UserException(ErrorMessage.USERNAME_READY_TAKEN);
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new UserException(ErrorMessage.EMAIL_READY_TAKEN);
        }
        User user = new User();
        String passwordEncrypt = encoder.encode(signUpRequest.getPassword());
        user.setPassword(passwordEncrypt);
        user.setAddress(signUpRequest.getAddress());
        user.setEmail(signUpRequest.getEmail());
        user.setUsername(signUpRequest.getUsername());
        userRepository.save(user);
    }

    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());
        String type = "Bearer";
        return new LoginResponse(jwt,
                type,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles,
                refreshToken.getToken()
        );
    }
}
