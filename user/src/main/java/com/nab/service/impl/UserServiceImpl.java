package com.nab.service.impl;

import com.nab.entity.Cart;
import com.nab.entity.User;
import com.nab.error.ErrorMessage;
import com.nab.exception.UserException;
import com.nab.payload.request.AddCartRequest;
import com.nab.payload.request.CartLineRequest;
import com.nab.payload.response.UserCartResponse;
import com.nab.payload.response.share.ProductResponse;
import com.nab.repository.UserRepository;
import com.nab.service.ProductService;
import com.nab.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ProductService productService;

    public UserServiceImpl(UserRepository userRepository, ProductService productService) {
        this.userRepository = userRepository;
        this.productService = productService;
    }

    @Override
    @Transactional
    public void addProductToCart(AddCartRequest request, Long userId) {

        List<Long> productIds = request.getCarts()
                .stream()
                .map(CartLineRequest::getProductId)
                .collect(Collectors.toList());
        List<ProductResponse> listProducts = productService.productList(productIds);

        validateCartInput(productIds, listProducts);

        Optional<User> userOptional = userRepository.findById(userId);
        processAddcart(request, userOptional);
    }

    /**
     * Merge cart from input of product of user with existing product in database
     * Example : In database user has product_id = 1 , quantity = 3, in request user add to cart product_id = 1 , quantity = 5
     * After add to cart in db has product_id = 1 , quantity = 8
     * @param request
     * @param userOptional
     */
    private void processAddcart(AddCartRequest request, Optional<User> userOptional) {
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            List<Cart> cartDbs = user.getCarts();
            List<Cart> carts =
                    request.getCarts().stream().map(c -> {
                        Cart cart = new Cart();
                        cart.setProductId(c.getProductId());
                        cart.setQuantity(c.getQuantity());
                        cart.setUser(user);
                        for(Cart cartdb : cartDbs) {
                            if(cartdb.getProductId().equals(c.getProductId())) {
                                cart.setQuantity(cartdb.getQuantity() + c.getQuantity());
                                cart.setId(cartdb.getId());
                                break;
                            }
                        }
                        return cart;
                    }).collect(Collectors.toList());
            user.setCarts(carts);
            userRepository.save(user);
        } else {
            log.error("User add product to cart is not exist");
            throw new UserException(ErrorMessage.INVALID_USER_ID);
        }
    }

    private void validateCartInput(List<Long> productIds, List<ProductResponse> listProducts) {
        List<Long> idNotMatch = differenceProduct(productIds, listProducts.stream().map(ProductResponse::getId).collect(Collectors.toList()));
        if (idNotMatch.size() > 0) {
            String ids = Arrays.toString(idNotMatch.toArray());
            throw new UserException(ErrorMessage.PRODUCT_ID_INVALID, "Product ID invalid " + ids);
        }
    }

    @Override
    public List<UserCartResponse> cart(Long userId) {
        Optional<User> users = userRepository.findById(userId);
        if(!users.isPresent()) {
            throw new UserException(ErrorMessage.INVALID_USER_ID);
        }
        List<Cart> carts = users.get().getCarts();
        Map<Long, Cart> mapCartByProduct = carts.stream().collect(Collectors.toMap(Cart::getProductId, Function.identity()));
        List<ProductResponse> listProducts = productService.productList(carts.stream().map(Cart::getProductId).collect(Collectors.toList()));
        return listProducts.stream().map(p -> {
            UserCartResponse userCartResponse = new UserCartResponse();
            userCartResponse.setProductResponse(p);
            Cart cart = mapCartByProduct.get(p.getId());
            userCartResponse.setCartId(cart.getId());
            userCartResponse.setQuantity(cart.getQuantity());
            return userCartResponse;
        }).collect(Collectors.toList());
    }

    List<Long> differenceProduct(List<Long> source, List<Long> dest) {
        return source.stream()
                .filter(element -> !dest.contains(element))
                .collect(Collectors.toList());
    }


}
