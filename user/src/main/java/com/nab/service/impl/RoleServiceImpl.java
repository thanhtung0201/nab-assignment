package com.nab.service.impl;

import com.nab.constant.ERole;
import com.nab.entity.Role;
import com.nab.entity.User;
import com.nab.error.ErrorMessage;
import com.nab.exception.UserException;
import com.nab.payload.request.AddRoleRequest;
import com.nab.repository.UserRepository;
import com.nab.service.RoleService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    private final UserRepository userRepository;

    public RoleServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public void addRoleUser(AddRoleRequest addRoleRequest) {
        Optional<User> user = userRepository.findById(addRoleRequest.getUserId());
        if(!user.isPresent()) {
            throw new UserException(ErrorMessage.INVALID_USER_ID);
        }
        if(user.get().getRoles().size() > 0 &&
                        user.get().
                                getRoles().stream()
                                .anyMatch(u -> u.getName().equals(ERole.ROLE_ADMIN)))
                        {
            throw new UserException(ErrorMessage.CHANGE_ROLE_ADMIN_INVALID);
        }
        if (addRoleRequest.getRole().equals(ERole.ROLE_ADMIN)) {
            throw new UserException(ErrorMessage.ADD_ROLE_ERROR);
        }
        User userSave = user.get();
        Role role = new Role(addRoleRequest.getUserId(), addRoleRequest.getRole());
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        userSave.setRoles(roles);
        userRepository.save(userSave);
    }
}
