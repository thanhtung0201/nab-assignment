package com.nab.controller;

import com.nab.payload.request.LoginRequest;
import com.nab.payload.request.SignupRequest;
import com.nab.payload.request.TokenRefreshRequest;
import com.nab.payload.response.LoginResponse;
import com.nab.payload.response.RefreshTokenResponse;
import com.nab.payload.response.base.BaseResponse;
import com.nab.service.AuthService;
import com.nab.service.RefreshTokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController extends BaseController {

    private final AuthService authService;
    private final RefreshTokenService refreshTokenService;


    public AuthController(AuthService authService, RefreshTokenService refreshTokenService) {
        this.authService = authService;
        this.refreshTokenService = refreshTokenService;
    }


    @PostMapping("/signup")
    public BaseResponse<String> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
            authService.register(signUpRequest);
            String successMessage = String.format("User %s registered successfully!", signUpRequest.getUsername());
            return ok(successMessage);
    }

    @PostMapping("/login")
    public BaseResponse<LoginResponse> login(@RequestBody LoginRequest loginRequest) {
        LoginResponse loginResponse = authService.login(loginRequest);
        return ok(loginResponse);
    }

    @PostMapping("/refresh_token")
    public BaseResponse<RefreshTokenResponse> refreshToken(@RequestBody TokenRefreshRequest request) {
        RefreshTokenResponse refreshTokenResponse = refreshTokenService.refreshToken(request);
        return ok(refreshTokenResponse);

    }



}
