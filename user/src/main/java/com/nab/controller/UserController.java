package com.nab.controller;

import com.nab.client.HttpClient;
import com.nab.payload.request.AddCartRequest;
import com.nab.payload.request.AddRoleRequest;
import com.nab.payload.response.UserCartResponse;
import com.nab.payload.response.base.BaseResponse;
import com.nab.service.RoleService;
import com.nab.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController  {

    private final RoleService roleService;
    private final UserService userService;

    public UserController(RoleService roleService, UserService userService) {
        this.roleService = roleService;
        this.userService = userService;
    }

    @PostMapping("/add-role")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public BaseResponse<String> addRole(@RequestBody AddRoleRequest addRoleRequest) {
        SecurityContextHolder.getContext().getAuthentication();
        roleService.addRoleUser(addRoleRequest);
        return ok(String.format("Add role %s to user user_id %s successfully", addRoleRequest.getRole(), addRoleRequest.getUserId()));
    }

    @PostMapping("/add-cart")
    public BaseResponse<String> addToCart(@RequestBody AddCartRequest addCartRequest) {
        Long userId = Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        userService.addProductToCart(addCartRequest, userId);
        return ok(String.format("Add cart for user %s successfully", ""));
    }

    @GetMapping("/my-cart")
    public BaseResponse<List<UserCartResponse>> myCart() {
        Long userId = Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        return ok(userService.cart(userId));
    }


}

