package com.nab.constant;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
