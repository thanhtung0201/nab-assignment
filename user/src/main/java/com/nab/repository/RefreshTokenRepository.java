package com.nab.repository;

import com.nab.entity.RefreshToken;
import com.nab.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

    @Override
    Optional<RefreshToken> findById(Long id);

    void deleteByUser(User entity);

    Optional<RefreshToken> findByToken(String token);

}
