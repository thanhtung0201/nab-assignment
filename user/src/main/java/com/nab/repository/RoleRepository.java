package com.nab.repository;

import com.nab.constant.ERole;
import com.nab.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface  RoleRepository extends JpaRepository<Role, Long> {
        Optional<Role> findByName(ERole name);
}
