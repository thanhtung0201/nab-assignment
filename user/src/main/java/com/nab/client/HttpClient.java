package com.nab.client;

import com.nab.error.ErrorMessage;
import com.nab.exception.UserException;
import com.nab.payload.response.base.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
@Slf4j
public class HttpClient {

    private final RestTemplate restTemplate;

    private final String productUrl;

    private final String PRODUCT_DETAIL_PATH = "/api/product/public/products";

    public HttpClient(RestTemplate restTemplate, @Value("${product.url}") String productUrl) {
        this.restTemplate = restTemplate;
        this.productUrl = productUrl;
    }

    public Object getProductDetail(List<Long> ids) {
        try {
            ResponseEntity<BaseResponse> response =
                    restTemplate
                            .postForEntity(productUrl + PRODUCT_DETAIL_PATH,
                                    ids,
                            BaseResponse.class);
            return response.getBody().getData();
        } catch (Exception ex) {
            log.error("Error when try to get product detail {}", ex.getMessage());
            throw new UserException(ErrorMessage.PRODUCT_ID_INVALID);
        }
    }

}
