package com.nab.exception;

import com.nab.payload.response.base.Error;
import com.nab.payload.response.base.MetaResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.*;

@ControllerAdvice
public class UserExceptionHandler {

    @ExceptionHandler(value
            = { Exception.class})
    protected ResponseEntity<Object> handleUserException (
            Exception ex, WebRequest request) {
        if (ex instanceof UserException) {
            return handleUserException((UserException) ex);
        }
        else if (ex instanceof BadCredentialsException || ex instanceof AccessDeniedException) {
            String code = "";
            HttpStatus httpStatus;
            String message;
            if(ex instanceof BadCredentialsException) {
                code = "BAD_CREDENTIAL";
                httpStatus = HttpStatus.UNAUTHORIZED;
                message = "Invalid access token";
            } else {
                code = "FORBIDDEN";
                httpStatus = HttpStatus.FORBIDDEN;
                message = "Access deny to this resource";
            }
            Error error = new
                    Error(code,
                    message, "USER_SERVICE");
            return ResponseEntity.status(httpStatus)
                    .body(new MetaResponse(httpStatus.value(),
                            Arrays.asList(error),
                            "USER_SERVICE",
                            null));
        }
        else if(ex instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
            List<Error> errorMessageResponses = new ArrayList<>();
            exception.getBindingResult().getAllErrors().forEach((error) -> {
                String fieldName = ((FieldError) error).getField();
                String errorMessage = error.getDefaultMessage();
                Error err = new Error(fieldName, errorMessage, "USER_SERVICE");
                errorMessageResponses.add(err);
            });
            return ResponseEntity.badRequest()
                    .body(new MetaResponse(HttpStatus.BAD_REQUEST.value(),
                            errorMessageResponses,
                            "USER_SERVICE",
                            null));
        }
        else {
            return ResponseEntity.internalServerError().body("System Exception , Please contact admin");
        }

    }

    private ResponseEntity<Object> handleUserException(UserException ex) {
        UserException userException = ex;
        MetaResponse metaResponse = new MetaResponse();
        Error error = new
                Error(userException.getError().getCode(),
                userException.getError().getMessage(), userException.getService());
        metaResponse.setErrors(Arrays.asList(error));
        metaResponse.setCode(userException.getHttpStatus());
        metaResponse.setService(error.getService());
        metaResponse.setExtraError(userException.getExtraError());
        return ResponseEntity.status(userException.getHttpStatus()).body(metaResponse);
    }


}
