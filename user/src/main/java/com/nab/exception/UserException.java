package com.nab.exception;

import com.nab.error.ErrorMessage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserException extends RuntimeException {

    private ErrorMessage error;
    private String service;
    private int httpStatus;
    private String extraError;


    public UserException(ErrorMessage error, String extraError) {
        this.error = error;
        this.service = "USER_SERVICE";
        this.httpStatus = 400;
        this.extraError = extraError;
    }

    public UserException(ErrorMessage error) {
        this.error = error;
        this.service = "USER_SERVICE";
        this.httpStatus = 400;
    }

    public UserException(ErrorMessage error, int httpStatus) {
        this.error = error;
        this.service = "USER_SERVICE";
        this.httpStatus = httpStatus;
    }
}
