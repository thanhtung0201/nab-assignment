package com.nab.entity;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "product")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    private String brand;

    private BigDecimal price;

    private String code;

    private String colour;

    @Column(name = "image_url")
    private String imageUrl;

    @ManyToOne
    @JoinColumn(name="discount_id", nullable=false)
    private Discount discount;

    @ManyToOne
    @JoinColumn(name="category_id", nullable=false)
    private Category category;


}
