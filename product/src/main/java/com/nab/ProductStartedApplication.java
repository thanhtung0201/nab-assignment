package com.nab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductStartedApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductStartedApplication.class, args);
    }
}
