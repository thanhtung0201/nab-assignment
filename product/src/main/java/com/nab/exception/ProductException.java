package com.nab.exception;

import com.nab.error.ErrorMessage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductException extends RuntimeException {

    private ErrorMessage error;
    private String service;
    private int httpStatus;

    public ProductException(ErrorMessage error) {
        this.error = error;
        this.service = "PRODUCT_SERVICE";
        this.httpStatus = 400;
    }

    public ProductException(ErrorMessage error, int httpStatus) {
        this.error = error;
        this.service = "PRODUCT_SERVICE";
        this.httpStatus = httpStatus;
    }
}
