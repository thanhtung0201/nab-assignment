package com.nab.service.impl;

import com.nab.entity.Discount;
import com.nab.error.ErrorMessage;
import com.nab.exception.ProductException;
import com.nab.payload.request.DiscountRequest;
import com.nab.repository.DiscountRepository;
import com.nab.service.DiscountService;
import org.springframework.stereotype.Service;

@Service
public class DiscountServiceImpl implements DiscountService {

    private final DiscountRepository discountRepository;

    public DiscountServiceImpl(DiscountRepository discountRepository) {
        this.discountRepository = discountRepository;
    }

    @Override
    public void createDiscount(DiscountRequest discountRequest) {
        if (discountRepository.existsByName(discountRequest.getName()))
            throw new ProductException(ErrorMessage.DISCOUNT_EXISTS);

        Discount discount = Discount.builder()
                .discountType(discountRequest.getDiscountType())
                .discountValue(discountRequest.getDiscountValue())
                .name(discountRequest.getName())
                .build();
        discountRepository.save(discount);
    }

}
