package com.nab.service.impl;

import com.nab.constant.Operation;
import com.nab.constant.ProductField;
import com.nab.entity.Category;
import com.nab.entity.Discount;
import com.nab.entity.Product;
import com.nab.error.ErrorMessage;
import com.nab.exception.ProductException;
import com.nab.payload.dto.SearchCriteria;
import com.nab.payload.request.FilterProductRequest;
import com.nab.payload.request.ProductRequest;
import com.nab.payload.response.ProductResponse;
import com.nab.repository.CategoryRepository;
import com.nab.repository.DiscountRepository;
import com.nab.repository.ProductRepository;
import com.nab.service.ProductService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final CategoryRepository categoryRepository;

    private final DiscountRepository discountRepository;


    public ProductServiceImpl(ProductRepository productRepository,
                              CategoryRepository categoryRepository,
                              DiscountRepository discountRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.discountRepository = discountRepository;
    }

    @Override
    @Transactional
    public void createProduct(ProductRequest productRequest) {
        if (productRepository.existsByName(productRequest.getName()))
            throw new ProductException(ErrorMessage.PRODUCT_EXISTS);
        Optional<Category> category = categoryRepository.findById(productRequest.getCategoryId());
        if (category.isEmpty())
            throw new ProductException(ErrorMessage.CATEGORY_ID_INVALID);

        Optional<Discount> discount = discountRepository.findById(productRequest.getDiscountId());
        if (discount.isEmpty())
            throw new ProductException(ErrorMessage.DISCOUNT_ID_INVALID);

        Product product = Product.builder().name(productRequest.getName())
                .code(productRequest.getCode())
                .brand(productRequest.getBrand())
                .colour(productRequest.getColour())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .imageUrl(productRequest.getImageUrl())
                .category(category.get())
                .discount(discount.get())
                .build();
        productRepository.save(product);
    }

    @Override
    public ProductResponse productDetail(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if(product.isEmpty()) {
            throw new ProductException(ErrorMessage.PRODUCT_NOT_FOUND, HttpStatus.NOT_FOUND.value());
        }
        return productToProductResponse(product.get());
    }

    @Override
    public List<ProductResponse> filterProduct(FilterProductRequest filterProductRequest) {
        List<ProductSpecification> specificationList = new ArrayList<>();
        if (filterProductRequest.getBrand() != null) {
            ProductSpecification productSpec = new ProductSpecification(new SearchCriteria(ProductField.BRAND, Operation.COLON, filterProductRequest.getBrand()));
            specificationList.add(productSpec);
        }
        if (filterProductRequest.getCode() != null) {
            ProductSpecification productSpec = new ProductSpecification(new SearchCriteria(ProductField.CODE, Operation.EQUAL, filterProductRequest.getCode()));
            specificationList.add(productSpec);
        }
        if (filterProductRequest.getColour() != null) {
            ProductSpecification productSpec = new ProductSpecification(new SearchCriteria(ProductField.COLOUR, Operation.EQUAL, filterProductRequest.getColour()));
            specificationList.add(productSpec);
        }
        if (filterProductRequest.getFromPrice() != null) {
            ProductSpecification productSpec = new ProductSpecification(new SearchCriteria(ProductField.PRICE, Operation.GREATER_THAN, filterProductRequest.getFromPrice()));
            specificationList.add(productSpec);
        }
        if (filterProductRequest.getToPrice() != null) {
            ProductSpecification productSpec = new ProductSpecification(new SearchCriteria(ProductField.PRICE, Operation.LESSER_THAN, filterProductRequest.getToPrice()));
            specificationList.add(productSpec);
        }
        if (filterProductRequest.getCategory() != null) {
            ProductSpecification productSpec = new ProductSpecification(new SearchCriteria(ProductField.CATEGORY, Operation.COLON, filterProductRequest.getCategory()));
            specificationList.add(productSpec);
        }
        if (filterProductRequest.getName() != null) {
            ProductSpecification productSpec = new ProductSpecification(new SearchCriteria(ProductField.NAME, Operation.COLON, filterProductRequest.getName()));
            specificationList.add(productSpec);
        }
        List<Product> productList;
        if(specificationList.size() > 0) {
            Specification<Product> productSpec;
            productSpec = Specification.where(specificationList.get(0));
            for(int i = 1; i < specificationList.size(); i++) {
                productSpec = productSpec.and(specificationList.get(i));
            }
            productList = productRepository.findAll(Specification.where(productSpec));
        } else {
            productList = productRepository.findAll();
        }
        return getProductResponses(productList);
    }

    @Override
    public List<ProductResponse> listProduct(List<Long> productIds) {
        List<Product> productList = productRepository.findAllById(productIds);
        return getProductResponses(productList);
    }

    private List<ProductResponse> getProductResponses(List<Product> productList) {
        return productList.stream().map(p -> {
            ProductResponse productResponse = new ProductResponse();
            productResponse.setBrand(p.getBrand());
            productResponse.setCategoryName(p.getCategory().getName());
            productResponse.setCode(p.getCode());
            productResponse.setColour(p.getColour());
            productResponse.setDescription(p.getDescription());
            productResponse.setId(p.getId());
            productResponse.setImageUrl(p.getImageUrl());
            productResponse.setDiscountValue(p.getDiscount() != null ? p.getDiscount().getDiscountValue() : null);
            productResponse.setPrice(p.getPrice());
            productResponse.setName(p.getName());
            return productResponse;
        }).collect(Collectors.toList());
    }


    private ProductResponse productToProductResponse(Product p) {
        return ProductResponse.builder().name(p.getName())
                .brand(p.getBrand())
                .code(p.getCode())
                .colour(p.getColour())
                .description(p.getDescription())
                .categoryName(p.getCategory().getName())
                .discountValue(p.getDiscount() != null ? p.getDiscount().getDiscountValue() : null)
                .imageUrl(p.getImageUrl())
                .id(p.getId())
                .price(p.getPrice()).build();

    }
}
