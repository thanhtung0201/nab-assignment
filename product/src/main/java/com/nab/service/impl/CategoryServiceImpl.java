package com.nab.service.impl;

import com.nab.entity.Category;
import com.nab.error.ErrorMessage;
import com.nab.exception.ProductException;
import com.nab.payload.request.CategoryRequest;
import com.nab.repository.CategoryRepository;
import com.nab.service.CategoryService;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void createCategory(CategoryRequest categoryRequest) {
        if (categoryRepository.existsByName(categoryRequest.getName()))
            throw new ProductException(ErrorMessage.CATEGORY_EXISTS);
        Category category = Category.builder()
                .name(categoryRequest.getName())
                .build();
        categoryRepository.save(category);
    }
}
