package com.nab.service.impl;

import com.nab.constant.Operation;
import com.nab.entity.Category;
import com.nab.entity.Product;
import com.nab.payload.dto.SearchCriteria;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

@AllArgsConstructor
public class ProductSpecification implements Specification<Product> {

    private final SearchCriteria searchCriteria;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if(searchCriteria.getKey().equals("category")) {
            Join<Product, Category> categoryListJoin = root.join("category");
            return criteriaBuilder.like(categoryListJoin.get("name"), "%" + searchCriteria.getValue() + "%");

        }
        else if(searchCriteria.getOperation().equalsIgnoreCase(Operation.COLON)) {
            if(root.get(searchCriteria.getKey()).getJavaType() == String.class) {
                return criteriaBuilder.like(root.get(searchCriteria.getKey()),
                        "%" + searchCriteria.getValue() + "%");
            } else return criteriaBuilder.equal(
                        root.get(searchCriteria.getKey()),
                        searchCriteria.getValue().toString());
            }
        else if(searchCriteria.getOperation().equalsIgnoreCase(Operation.GREATER_THAN)) {
            return criteriaBuilder.greaterThanOrEqualTo(root.get(searchCriteria.getKey()),
                    searchCriteria.getValue().toString());
        }
        else if(searchCriteria.getOperation().equalsIgnoreCase(Operation.LESSER_THAN)) {
            return criteriaBuilder.lessThanOrEqualTo(root.get(searchCriteria.getKey()),
                    searchCriteria.getValue().toString());
        } else
            return criteriaBuilder.equal(root.get(searchCriteria.getKey()),
                    searchCriteria.getValue().toString());
    }
}
