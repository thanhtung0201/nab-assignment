package com.nab.service;

import com.nab.payload.request.DiscountRequest;

public interface DiscountService {

    /**
     * Create discount
     * @param discountRequest {
     *        discountType , name : required , discountValue
     * }
     */
    void createDiscount(DiscountRequest discountRequest);

}
