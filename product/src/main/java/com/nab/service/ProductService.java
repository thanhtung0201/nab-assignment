package com.nab.service;

import com.nab.payload.request.FilterProductRequest;
import com.nab.payload.request.ProductRequest;
import com.nab.payload.response.ProductResponse;

import java.util.List;

public interface ProductService {

    /**
     * 
     * @param productRequest
     * Request create product
     */
    void createProduct(ProductRequest productRequest);

    /**
     * Get Product Detail base on Id
     * @param id
     * @return
     */
    ProductResponse productDetail(Long id);

    /**
     * Search and filter product base on some condition
     * @param request
     * @return
     */
    List<ProductResponse> filterProduct(FilterProductRequest request);

    /**
     * Get list of product detail
     * @param productIds
     * @return
     */
    List<ProductResponse> listProduct(List<Long> productIds);

}
