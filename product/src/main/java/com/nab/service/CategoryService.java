package com.nab.service;

import com.nab.payload.request.CategoryRequest;

public interface CategoryService {

    /**
     * Create category
     * @param categoryRequest {name : required}
     */
    void createCategory(CategoryRequest categoryRequest);

}
