package com.nab.controller;

import com.nab.payload.request.CategoryRequest;
import com.nab.payload.request.DiscountRequest;
import com.nab.payload.request.ProductRequest;
import com.nab.payload.response.base.BaseResponse;
import com.nab.service.CategoryService;
import com.nab.service.DiscountService;
import com.nab.service.ProductService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/internal")
public class ProductInternalController extends BaseController {

    private final ProductService productService;

    private final CategoryService categoryService;

    private final DiscountService discountService;

    public ProductInternalController(ProductService productService,
                                     CategoryService categoryService,
                                     DiscountService discountService) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.discountService = discountService;
    }

    /**
     * Admin create product
     * @param productRequest
     * @return
     */
    @PostMapping("/product")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public BaseResponse<String> createProduct(@Valid @RequestBody ProductRequest productRequest) {
        productService.createProduct(productRequest);
        String successMessage = String.format("Create %s product successfully!", productRequest.getName());
        return ok(successMessage);
    }

    /**
     * Admin create category
     * @param categoryRequest
     * @return
     */
    @PostMapping("/category")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public BaseResponse<String> createCategory(@Valid @RequestBody CategoryRequest categoryRequest) {
        categoryService.createCategory(categoryRequest);
        String successMessage = String.format("Create %s category successfully!", categoryRequest.getName());
        return ok(successMessage);
    }


    /**
     * Admin create discount
     * @param discountRequest
     * @return
     */
    @PostMapping("/discount")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public BaseResponse<String> createDiscount(@Valid @RequestBody DiscountRequest discountRequest) {
        discountService.createDiscount(discountRequest);
        String successMessage = String.format("Create %s discount successfully!", discountRequest.getName());
        return ok(successMessage);
    }

}
