package com.nab.controller;

import com.nab.entity.Product;
import com.nab.payload.dto.SearchCriteria;
import com.nab.payload.request.CategoryRequest;
import com.nab.payload.request.FilterProductRequest;
import com.nab.payload.response.ProductResponse;
import com.nab.payload.response.base.BaseResponse;
import com.nab.repository.ProductRepository;
import com.nab.service.ProductService;
import com.nab.service.impl.ProductSpecification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/product/public")
@Slf4j
public class ProductController extends BaseController {

    private final ProductService productService;


    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    /**
     * Get detail of product
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public BaseResponse<ProductResponse> productDetail(@PathVariable Long id) {
        log.info("Request to get product {}", id);
        return ok(productService.productDetail(id));
    }

    /**
     * Filter product
     * @param filterProductRequest
     * @return
     */
    @PostMapping("/filter")
    public BaseResponse<List<ProductResponse>> productFilter(@RequestBody FilterProductRequest filterProductRequest) {
        log.info("Filter product with body params {} ", filterProductRequest.toString());
        return ok(productService.filterProduct(filterProductRequest));
    }

    /**
     * Get list of product base on arrays of product_id
     * @param productIds
     * @return
     */
    @PostMapping("/products")
    public BaseResponse<List<ProductResponse>> productList(@RequestBody List<Long> productIds) {
        return ok(productService.listProduct(productIds));
    }


}
