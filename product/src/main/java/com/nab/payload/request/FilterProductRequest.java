package com.nab.payload.request;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FilterProductRequest {

    private BigDecimal fromPrice;
    private BigDecimal toPrice;
    private String brand;
    private String code;
    private String colour;
    private String name;
    private String category;

}
