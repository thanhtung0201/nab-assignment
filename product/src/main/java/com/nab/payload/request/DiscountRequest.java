package com.nab.payload.request;

import com.nab.constant.DiscountType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Getter
@Setter
public class DiscountRequest {

    private DiscountType discountType;

    @NotBlank
    private String name;

    @Positive
    private BigDecimal discountValue;

}
