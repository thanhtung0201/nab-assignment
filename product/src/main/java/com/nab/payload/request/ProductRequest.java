package com.nab.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
public class ProductRequest {

    @NotBlank
    private String name;

    @NotBlank
    @Size(max = 50)
    private String code;

    private String imageUrl;

    private Integer discountId;

    private Integer categoryId;

    private String description;

    private String brand;

    @Positive
    private BigDecimal price;

    private String colour;

}
