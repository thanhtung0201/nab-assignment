package com.nab.constant;

public class Operation {
    public static final String EQUAL = "=";
    public static final String GREATER_THAN = ">=";
    public static final String LESSER_THAN = "<=";
    public static final String COLON = ":";

}
