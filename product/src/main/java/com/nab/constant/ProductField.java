package com.nab.constant;

public class ProductField {

    public static final String BRAND = "brand";
    public static final String CODE = "code";
    public static final String COLOUR = "colour";
    public static final String PRICE = "price";
    public static final String CATEGORY = "category";
    public static final String NAME = "name";
}
