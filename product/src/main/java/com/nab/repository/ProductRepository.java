package com.nab.repository;

import com.nab.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaSpecificationExecutor<Product>, JpaRepository<Product, Long> {

    Boolean existsByName(String name);
}
