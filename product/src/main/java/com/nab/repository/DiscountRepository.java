package com.nab.repository;

import com.nab.entity.Category;
import com.nab.entity.Discount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscountRepository extends JpaRepository<Discount, Integer> {

    Boolean existsByName(String name);

}
