package com.nab.service;

import com.nab.entity.Category;
import com.nab.entity.Discount;
import com.nab.error.ErrorMessage;
import com.nab.exception.ProductException;
import com.nab.payload.request.ProductRequest;
import com.nab.repository.CategoryRepository;
import com.nab.repository.DiscountRepository;
import com.nab.repository.ProductRepository;
import com.nab.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ProductServiceTest {

    @InjectMocks
    @Spy
    private ProductServiceImpl productService;

    @Mock
    ProductRepository productRepository;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    DiscountRepository discountRepository;

    ProductRequest requestMock;

    @BeforeEach
    void before(){
        requestMock = new ProductRequest();
        requestMock.setName("Product Name");
    };

    @DisplayName("Required dependency")
    @Test
    public void mustHaveDependency() {
        Assertions.assertNotNull(productRepository);
        Assertions.assertNotNull(categoryRepository);
        Assertions.assertNotNull(discountRepository);
    }

    @Test
    @DisplayName("Create Product When Name is exists")
    void testCreateProduct_whenProductNameExists() {
        when(productRepository.existsByName(Mockito.anyString())).thenReturn(true);
        ProductException exception = assertThrows(ProductException.class, () -> {
            productService.createProduct(requestMock);
        });
        assertEquals(exception.getError(), ErrorMessage.PRODUCT_EXISTS);
    }

    @Test
    @DisplayName("Create Product When Category is not exists")
    void testCreateProduct_whenCategoryNotExists() {
        when(categoryRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        ProductException exception = assertThrows(ProductException.class, () -> {
            productService.createProduct(requestMock);
        });
        assertEquals(exception.getError(), ErrorMessage.CATEGORY_ID_INVALID);
    }

    @Test
    @DisplayName("Create Product When Discount ID is not exists")
    void testCreateProduct_whenDiscountNotExists() {
        when(categoryRepository.findById(Mockito.any()))
                .thenReturn(Optional.of(new Category()));
        when(discountRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        ProductException exception = assertThrows(ProductException.class, () -> {
            productService.createProduct(requestMock);
        });
        assertEquals(exception.getError(), ErrorMessage.DISCOUNT_ID_INVALID);
    }

    @Test
    @DisplayName("Create product successfully")
    public void testCreateProduct_Success()  {
        when(categoryRepository.findById(Mockito.any()))
                .thenReturn(Optional.of(new Category()));
        when(discountRepository.findById(Mockito.any())).thenReturn(Optional.of(new Discount()));
        ProductRequest productRequest = new ProductRequest();
        productRequest.setName("Name");
        productRequest.setCode("Code");
        productRequest.setBrand("Brand");
        productRequest.setColour("Colour");
        productRequest.setDescription("Description");
        productRequest.setImageUrl("Image Url");
        productRequest.setDiscountId(3);
        productRequest.setCategoryId(2);
        Assertions.assertDoesNotThrow(() -> productService.createProduct(productRequest));
    }




}
