package com.nab.service;

import com.nab.entity.Discount;
import com.nab.error.ErrorMessage;
import com.nab.exception.ProductException;
import com.nab.payload.request.CategoryRequest;
import com.nab.payload.request.DiscountRequest;
import com.nab.repository.CategoryRepository;
import com.nab.repository.DiscountRepository;
import com.nab.service.impl.CategoryServiceImpl;
import com.nab.service.impl.DiscountServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.jdbc.BadSqlGrammarException;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class DiscountServiceTest {

    @InjectMocks
    @Spy
    private DiscountServiceImpl discountService;

    @Mock
    DiscountRepository discountRepository;

    @DisplayName("Required dependency")
    @Test
    public void mustHaveDependency() {
        Assertions.assertNotNull(discountRepository);
    }

    private ArgumentMatcher<Discount> discountValueInvalid() {
        return discount -> discount != null && discount.getDiscountValue().compareTo(BigDecimal.ZERO) < 0;
    }

    @Test
    @DisplayName("Create Discount When Name is exists")
    void testCreateDiscount_whenDiscountNameExists() {
        DiscountRequest discountRequest = new DiscountRequest();
        discountRequest.setName("SHOPEE 9/9");
        when(discountRepository.existsByName("SHOPEE 9/9")).thenReturn(true);
        ProductException exception = assertThrows(ProductException.class, () -> {
            discountService.createDiscount(discountRequest);
        });
        assertEquals(exception.getError(), ErrorMessage.DISCOUNT_EXISTS);
    }

    @Test
    @DisplayName("Create Discount When Discount Value is invalid")
    void testCreateDiscount_whenDiscountValueInvalid() {
        DiscountRequest discountRequest = new DiscountRequest();
        discountRequest.setName("SHOPEE 9/9");
        discountRequest.setDiscountValue(new BigDecimal(-230000));
        when(discountRepository.existsByName(Mockito.anyString())).thenReturn(false);

        when(discountRepository.save(
                argThat(discountValueInvalid())
        )).thenThrow(new ProductException(ErrorMessage.AMOUNT_INVALID));
        ProductException productException = Assertions.assertThrows(ProductException.class, () -> discountService.createDiscount(discountRequest));
        assertEquals(productException.getError(), ErrorMessage.AMOUNT_INVALID);
    }



    @Test
    @DisplayName("Create Discount Successfully")
    void testCreateDiscountSuccess() {
        DiscountRequest discountRequest = new DiscountRequest();
        discountRequest.setName("SHOPEE 9/9");
        when(discountRepository.existsByName(Mockito.anyString())).thenReturn(false);
        discountRequest.setDiscountValue(new BigDecimal(230000));
        when(discountRepository.existsByName(Mockito.anyString())).thenReturn(false);

        when(discountRepository.save(
                argThat(discountValueInvalid())
        )).thenThrow(new ProductException(ErrorMessage.AMOUNT_INVALID));
        Assertions.assertDoesNotThrow(() -> discountService.createDiscount(discountRequest));
    }

}
