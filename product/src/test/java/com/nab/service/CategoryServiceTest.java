package com.nab.service;

import com.nab.error.ErrorMessage;
import com.nab.exception.ProductException;
import com.nab.payload.request.CategoryRequest;
import com.nab.repository.CategoryRepository;
import com.nab.service.impl.CategoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CategoryServiceTest {

    @InjectMocks
    @Spy
    private CategoryServiceImpl categoryService;

    @Mock
    CategoryRepository categoryRepository;

    @DisplayName("Required dependency")
    @Test
    public void mustHaveDependency() {
        Assertions.assertNotNull(categoryRepository);
    }

    @Test
    @DisplayName("Create Category When Name is exists")
    void testCreateCategory_whenCategoryNameExists() {
        CategoryRequest categoryRequest = new CategoryRequest();
        categoryRequest.setName("CAR");
        when(categoryRepository.existsByName("CAR")).thenReturn(true);
        ProductException exception = assertThrows(ProductException.class, () -> {
            categoryService.createCategory(categoryRequest);
        });
        assertEquals(exception.getError(), ErrorMessage.CATEGORY_EXISTS);
    }

    @Test
    @DisplayName("Create Category Successfully")
    void testCreateCategorySuccess() {
        CategoryRequest categoryRequest = new CategoryRequest();
        categoryRequest.setName("CAR");
        when(categoryRepository.existsByName("CAR")).thenReturn(false);
        Assertions.assertDoesNotThrow(() -> categoryService.createCategory(categoryRequest));
    }

}
