package com.nab.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nab.payload.request.CategoryRequest;
import com.nab.payload.request.DiscountRequest;
import com.nab.payload.request.ProductRequest;
import com.nab.payload.response.base.BaseResponse;
import com.nab.service.CategoryService;
import com.nab.service.DiscountService;
import com.nab.service.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;

import static org.mockito.Mockito.doNothing;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ProductInternalControllerTest {

    public static final String API_INTERNAL = "/api/internal";
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService categoryService;

    @MockBean
    private DiscountService discountService;

    @MockBean
    private ProductService productService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("CreateProduct_WhenNameOrCodeBlank")
    public void createProductNameOrCodeBlank() throws Exception {
        ProductRequest request = new ProductRequest();
        mockMvc.perform(MockMvcRequestBuilders.post(API_INTERNAL + "/product")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @DisplayName("CreateProduct_WhenPriceNegative")
    public void createProductPriceNegative() throws Exception {
        ProductRequest request = new ProductRequest();
        mockMvc.perform(MockMvcRequestBuilders.post(API_INTERNAL + "/product")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @DisplayName("CreateProductSuccess_WhenProductRequestValid")
    @WithMockUser(username="admin",roles={"ADMIN"})
    public void createProductSuccess() throws Exception {
        ProductRequest request = new ProductRequest();
        request.setName("IPHONE 12");
        request.setCode("IPHONE 12");
        request.setPrice(new BigDecimal(100));
        doNothing().when(productService).createProduct(request);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(API_INTERNAL + "/product")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        BaseResponse<String> response = objectMapper.readValue(contentAsString, BaseResponse.class);
        Assertions.assertEquals(String.format("Create %s product successfully!", request.getName()),
                response.getData());

    }

    @Test
    @DisplayName("CreateCategory_WhenNameBlank")
    public void createCategoryNameBlank() throws Exception {
        CategoryRequest request = new CategoryRequest();
        mockMvc.perform(MockMvcRequestBuilders.post(API_INTERNAL + "/category")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    @Test
    @DisplayName("CreateCategorySuccess_WhenCategoryRequestValid")
    @WithMockUser(username="admin",roles={"ADMIN"})
    public void createCategorySuccess() throws Exception {
        CategoryRequest request = new CategoryRequest();
        request.setName("CAR");
        doNothing().when(categoryService).createCategory(request);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(API_INTERNAL + "/category")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        BaseResponse<String> response = objectMapper.readValue(contentAsString, BaseResponse.class);
        Assertions.assertEquals(String.format("Create %s category successfully!", request.getName()),
                response.getData());
    }


    @Test
    @DisplayName("CreateDiscount_WhenNameBlank")
    public void createDiscountNameBlank() throws Exception {
        DiscountRequest request = new DiscountRequest();
        mockMvc.perform(MockMvcRequestBuilders.post(API_INTERNAL + "/discount")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @DisplayName("CreateDiscount_WhenDiscountValueNegative")
    public void createDiscountValueNegative() throws Exception {
        DiscountRequest request = new DiscountRequest();
        request.setDiscountValue(new BigDecimal(-19));
        mockMvc.perform(MockMvcRequestBuilders.post(API_INTERNAL + "/discount")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @DisplayName("CreateDiscountSuccess_WhenDiscountRequestValid")
    @WithMockUser(username="admin",roles={"ADMIN"})
    public void createDiscountSuccess() throws Exception {
        DiscountRequest request = new DiscountRequest();
        request.setName("SHOPEE");
        request.setDiscountValue(new BigDecimal(100));
        doNothing().when(discountService).createDiscount(request);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(API_INTERNAL + "/discount")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        BaseResponse<String> response = objectMapper.readValue(contentAsString, BaseResponse.class);
        Assertions.assertEquals(String.format("Create %s discount successfully!", request.getName()),
        response.getData());

    }



}
