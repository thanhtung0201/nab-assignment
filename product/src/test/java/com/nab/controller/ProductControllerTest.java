package com.nab.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nab.payload.request.FilterProductRequest;
import com.nab.payload.response.ProductResponse;
import com.nab.payload.response.base.BaseResponse;
import com.nab.service.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

    public static final String API_PRODUCT_PUBLIC = "/api/product/public";
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Autowired
    private ObjectMapper objectMapper;



    @Test
    @DisplayName("Get Product Detail")
    public void getProductDetail() throws Exception {
        Long id = 5L;
        ProductResponse expected = new ProductResponse();
        expected.setName("IPHONE 11");
        expected.setPrice(new BigDecimal(200));
        expected.setColour("RED");
        expected.setCode("APPLE");
        expected.setId(id);
        given(productService.productDetail(id)).willReturn(expected);
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(API_PRODUCT_PUBLIC + "/{id}", id)
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        BaseResponse<Object> response = objectMapper.readValue(contentAsString, BaseResponse.class);
        Object data = response.getData();
        ProductResponse r = objectMapper.convertValue(data,
                new TypeReference<>() {
                });
        Assertions.assertEquals(r.getName(), expected.getName());
        Assertions.assertEquals(r.getCode(), expected.getCode());
        Assertions.assertEquals(r.getColour(), expected.getColour());
        Assertions.assertEquals(r.getId(), id);
    }

    @Test
    @DisplayName("Filter Product")
    public void filterProduct() throws Exception {
        Long id = 5L;
        ProductResponse expected = new ProductResponse();
        expected.setName("IPHONE 11");
        expected.setPrice(new BigDecimal(200));
        expected.setColour("RED");
        expected.setCode("APPLE");
        expected.setId(id);
        FilterProductRequest filterProductRequest = new FilterProductRequest();
        filterProductRequest.setCode("AP");
        filterProductRequest.setColour("RED");
        filterProductRequest.setName("IPHONE");
        filterProductRequest.setFromPrice(new BigDecimal(40));
        filterProductRequest.setToPrice(new BigDecimal(880));
        filterProductRequest.setBrand(null);
        filterProductRequest.setCategory(null);
        given(productService.filterProduct(Mockito.argThat(filterProductMatcher(filterProductRequest)))).willReturn(Collections.singletonList(expected));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(API_PRODUCT_PUBLIC + "/filter")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(filterProductRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        BaseResponse<Object> response = objectMapper.readValue(contentAsString, BaseResponse.class);
        Object data = response.getData();
        List<ProductResponse> rs = objectMapper.convertValue(data,
                new TypeReference<>() {
                });
        ProductResponse r = rs.get(0);
        Assertions.assertEquals(r.getName(), expected.getName());
        Assertions.assertEquals(r.getCode(), expected.getCode());
        Assertions.assertEquals(r.getColour(), expected.getColour());
        Assertions.assertEquals(r.getId(), id);
    }

    private ArgumentMatcher<FilterProductRequest> filterProductMatcher(FilterProductRequest filter) {
        return pd -> pd != null && filter != null &&
                pd.getName().contains(filter.getName()) &&
                pd.getColour().equals(filter.getColour()) &&
                pd.getCode().contains(filter.getCode()) &&
                pd.getFromPrice().equals(filter.getFromPrice()) &&
                pd.getToPrice().equals(filter.getToPrice());
    }

}
